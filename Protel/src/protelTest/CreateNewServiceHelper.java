package protelTest;

public class CreateNewServiceHelper {
    //variables
    public static final String username = "radu.marinescu@iello.ro";
    public static final String password = "xxxx";
    public static final String approvalCodeValue = "hffkJypdGBPabhGqWfhs";
    public static final String serviceIdentifier = "serviceIdentifier1";
    public static final String serviceName = "service1";
    public static final String integrationTypeSelection = "CMS";
    //constants
    public static final String signInIcon = "/html/body/header/nav/div[3]/ul/ul/li[2]/a/i";
    public static final String usernameField = "//*[@id=\"username\"]";
    public static final String passwordField = "//*[@id=\"password\"]";
    public static final String sideBarElement1 = "//*[@id=\"left-sidebar\"]/aside/section/aside/section/ul[2]/li[1]/a/span[1]";
    public static final String myAccountIcon = "/html/body/header/nav/div/ul/li[6]/a/span";
    public static final String developmentButton = "/html/body/header/nav/div/ul/li[1]/a";
    public static final String createNewServiceButton = "/html/body/header/nav/div/ul/li[1]/ul/li[1]/a";
    public static final String linkNDA = "//*[@id=\"ndaTerms\"]";
    public static final String firstNameNDA = "//*[@id=\"firstName\"]";
    public static final String lastNameNDA = "//*[@id=\"lastName\"]";
    public static final String companyNDA = "//*[@id=\"company2\"]";
    public static final String acceptButtonNDA = "//SPAN[@id='firstName']/../../../..//INPUT[@id='chkAgree']";
    public static final String createNewTeam = "/html/body/div[1]/div/div[2]/div[2]/div/section[2]/form/div[1]/div[4]/div/div[1]/div[1]/div/div/div/ul/li/a";
    public static final String approvalCodeField = "//INPUT[@id='approvalCode']";
    public static final String developmentTeamMenu = "(//BUTTON[@type='button'])[4]";
    public static final String serviceIdentifierField = "//INPUT[@id='serviceIdentifier']";
    public static final String serviceNameField = "//INPUT[@id='serviceName']";
    public static final String integrationTypeSelect = "(//BUTTON[@type='button'])[6]";
    public static final String IntegrationSelection = "(//INPUT[@type='checkbox'])[3]";
    public static final String label = "//LABEL[@for='selectServiceType'][text()='Integration type']";
    public static final String createNewServiceButton1 = "//BUTTON[@id='createService']";


}
