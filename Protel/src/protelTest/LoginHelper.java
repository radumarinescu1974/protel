package protelTest;

public class LoginHelper {
    //variables
    public static final String username = "radu.marinescu@iello.ro";
    public static final String password = "xxxx";
    //changePassword
    public static final String newPassword = "Mujix22";
    public static final String retypePassword = "Mujix22";
    //constants
    public static final String signinIcon = "/html/body/header/nav/div[3]/ul/ul/li[2]/a";
    public static final String usernameField = "//*[@id=\"username\"]";
    public static final String passwordField = "//*[@id=\"password\"]";
    public static final String signInButton = "//*[@id=\"loginBtn\"]";
    public static final String myAccountIcon = "/html/body/header/nav/div/ul/li[6]/a/span";
    public static final String signoutButton = "//*[@id=\"logout-link\"]";
    public static final String myAccountButton = "/html/body/header/nav/div/ul/li[6]/ul/div[1]/a";
    public static final String currentPasswordField = "//*[@id=\"currentPassword\"]";
    public static final String newPasswordField = "//*[@id=\"newPassword\"]";
    public static final String retypePasswordField = "//*[@id=\"newPasswordConfirm\"]";
    public static final String changePassSubmit = "//*[@id=\"change-password\"]/div[5]/div/input";
}
