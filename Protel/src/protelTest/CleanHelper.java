package protelTest;

public class CleanHelper {
    //variables
    public static final String carbonAmdevUrl = "https://amdev.protel.io:9443/carbon/admin/login.jsp";
    public static final String carbonProdUrl = "https://172.31.187.140:9443/carbon/admin/login.jsp";
    public static final String carbonUsername = "admin@wso2.com";
    public static final String carbonAmdevPassword = "xxxxx";
    public static final String carbonProdPassword = "xxxxx";
    public static final String userToClean = "radu.marinescu@iello.ro";
    //constants
    public static final String carbonUsernameField = "//*[@id=\"txtUserName\"]";
    public static final String carbonPasswordField = "//*[@id=\"txtPassword\"]";
    public static final String carbonSigninButtom = "//*[@id=\"loginbox\"]/form/table/tbody/tr[4]/td[2]/input";
    public static final String carbonUserList = "//*[@id=\"menu\"]/ul/li[3]/ul/li[2]/ul/li[2]/a";
    public static final String carbonUsers = "//*[@id=\"internal\"]/tbody/tr[1]/td/a";
    public static final String usernameSearch = "//*[@id=\"workArea\"]/form[1]/table/tbody/tr[2]/td[2]/input[1]";
    public static final String cleanSearchButton = "//*[@id=\"workArea\"]/form[1]/table/tbody/tr[2]/td[2]/input[2]";
    public static final String deleteButton = "//*[@id=\"userTable\"]/tbody/tr/td[2]/a[4]";
    public static final String yesButton = "/html/body/div[3]/div[2]/button[1]";
    public static final String closeButton1 = "/html/body/div[5]/div[1]/div[1]/a";
    public static final String closeButton2 = "/html/body/div[3]/div[1]/div[1]/a";
}
