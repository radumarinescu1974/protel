package protelTest;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.awt.*;
import java.util.concurrent.TimeUnit;
import protelTest.SMPV2BulkTransferServiceHelper;




public class SMPTestAutomation {
    public WebDriver driver;
    public static void safeSleep(long sleep) {
        try {
            Thread.sleep(sleep);
        } catch (Exception e) {
        }
    }
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "/Users/radu/Downloads/chromedriver");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    //@BeforeClass
    @Test
    public void login() throws InterruptedException {
        driver.findElement(By.xpath(LoginHelper.signinIcon)).click();
        driver.switchTo().activeElement();
        sendKeysUsingAngularHack(LoginHelper.usernameField, LoginHelper.username);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath(LoginHelper.passwordField)).sendKeys(LoginHelper.password);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath(LoginHelper.signInButton)).click();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.switchTo().activeElement();
        Thread.sleep(5000);
        driver.findElement(By.xpath(LoginHelper.myAccountIcon)).click();
        driver.findElement(By.xpath(LoginHelper.signoutButton)).click();
        System.out.println("Login Successfull Tested");
    }

    @Test
    public void clearUserDev() throws InterruptedException {
        driver.navigate().to(CleanHelper.carbonAmdevUrl);
        sendKeysUsingAngularHack(CleanHelper.carbonUsernameField, CleanHelper.carbonUsername);
        sendKeysUsingAngularHack(CleanHelper.carbonPasswordField, CleanHelper.carbonAmdevPassword);
        driver.findElement(By.xpath(CleanHelper.carbonSigninButtom)).click();
        driver.findElement(By.xpath(CleanHelper.carbonUserList)).click();
        driver.findElement(By.xpath(CleanHelper.carbonUsers)).click();
        driver.findElement(By.xpath(CleanHelper.usernameSearch)).clear();
        sendKeysUsingAngularHack(CleanHelper.usernameSearch, CleanHelper.userToClean);
        driver.findElement(By.xpath(CleanHelper.cleanSearchButton)).click();
        driver.findElement(By.xpath(CleanHelper.deleteButton)).click();
        driver.findElement(By.xpath(CleanHelper.yesButton)).click();
        driver.findElement(By.xpath(CleanHelper.closeButton1)).click();
        driver.findElement(By.xpath(CleanHelper.closeButton2)).click();
        System.out.println("User successfully deleted");

    }

    @Test
    public void changePassword() throws InterruptedException {
        driver.findElement(By.xpath(LoginHelper.signinIcon)).click();
        driver.switchTo().activeElement();
        sendKeysUsingAngularHack(LoginHelper.usernameField, LoginHelper.username);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath(LoginHelper.passwordField)).sendKeys(LoginHelper.password);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath(LoginHelper.signInButton)).click();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.switchTo().activeElement();
        Thread.sleep(5000);
        //assertion_method_1(sideBarElement1,"About SOAP APIs");
        driver.findElement(By.xpath(LoginHelper.myAccountIcon)).click();
        driver.findElement(By.xpath(LoginHelper.myAccountButton)).click();
        sendKeysUsingAngularHack(LoginHelper.currentPasswordField, LoginHelper.password);
        sendKeysUsingAngularHack(LoginHelper.newPasswordField, LoginHelper.newPassword);
        sendKeysUsingAngularHack(LoginHelper.retypePasswordField, LoginHelper.retypePassword);
        driver.findElement(By.xpath(LoginHelper.changePassSubmit)).click();
        System.out.println("Password Changed Successfull");
    }

    @Test
    public void createNewService() throws InterruptedException {
        driver.findElement(By.xpath(CreateNewServiceHelper.signInIcon)).click();
        driver.switchTo().activeElement();
        sendKeysUsingAngularHack(CreateNewServiceHelper.usernameField, CreateNewServiceHelper.username);
        sendKeysUsingAngularHack(CreateNewServiceHelper.passwordField, CreateNewServiceHelper.password);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.findElement(By.id("loginBtn")).click();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.switchTo().activeElement();
        Thread.sleep(5000);
        assertion_method_1(CreateNewServiceHelper.sideBarElement1, "About SOAP APIs");
        driver.findElement(By.xpath(CreateNewServiceHelper.developmentButton)).click();
        driver.findElement(By.xpath(CreateNewServiceHelper.createNewServiceButton)).click();
        driver.findElement(By.xpath(CreateNewServiceHelper.linkNDA)).click();
        Thread.sleep(5000);
        assertion_method_1(CreateNewServiceHelper.firstNameNDA, RegisterHelper.firstName);
        assertion_method_1(CreateNewServiceHelper.lastNameNDA, RegisterHelper.lastName);
        assertion_method_1(CreateNewServiceHelper.companyNDA, RegisterHelper.companyName);
        Thread.sleep(6000);
        driver.findElement(By.xpath(CreateNewServiceHelper.acceptButtonNDA)).click();
        Thread.sleep(5000);
        sendKeysUsingAngularHack(CreateNewServiceHelper.approvalCodeField, CreateNewServiceHelper.approvalCodeValue);
        sendKeysUsingAngularHack(CreateNewServiceHelper.serviceIdentifierField, CreateNewServiceHelper.serviceIdentifier);
        sendKeysUsingAngularHack(CreateNewServiceHelper.serviceNameField, CreateNewServiceHelper.serviceName);


        WebElement integrationType = driver.findElement(By.xpath(CreateNewServiceHelper.integrationTypeSelect));
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + integrationType.getLocation().y + ")");
        integrationType.click();
//        driver.findElement(By.xpath(CreateNewServiceHelper.integrationTypeSelect)).click();
        WebElement integrationselection=driver.findElement(By.xpath(CreateNewServiceHelper.IntegrationSelection));
        integrationselection.click();
        driver.findElement(By.xpath(CreateNewServiceHelper.serviceNameField)).click();
//        integrationselection.submit();
//        driver.findElement(By.xpath(CreateNewServiceHelper.label)).click();
        driver.findElement(By.xpath(CreateNewServiceHelper.createNewServiceButton1)).click();
    }

    @Test
    public void registerFail() throws InterruptedException {
        driver.findElement(By.xpath(RegisterHelper.signupIcon)).click();
        driver.switchTo().activeElement();
        sendKeysUsingAngularHack(RegisterHelper.emailField, RegisterHelper.emailAddress);
        sendKeysUsingAngularHack(RegisterHelper.firstNameField, RegisterHelper.firstName);
        sendKeysUsingAngularHack(RegisterHelper.lastNameField, RegisterHelper.lastName);
        sendKeysUsingAngularHack(RegisterHelper.passwordField, RegisterHelper.password);
        sendKeysUsingAngularHack(RegisterHelper.retypePasswordField, RegisterHelper.retypePassword);
        sendKeysUsingAngularHack(RegisterHelper.companyNameField, RegisterHelper.companyName);
        sendKeysUsingAngularHack(RegisterHelper.countrySelector, RegisterHelper.country);
        driver.findElement(By.xpath(RegisterHelper.signupButton)).click();
        //System.out.println(driver.getPageSource());
        Thread.sleep(3999);
        //asertu' de culoare
//		assert(driver.getPageSource().contains("User name already exists"));
        assert (driver.findElement(By.xpath(RegisterHelper.signupButton)).isDisplayed());
        //*[@id="sign-up"]/div[4]
    }

    @Test
    public void register() throws InterruptedException {
        driver.findElement(By.xpath(RegisterHelper.signupIcon)).click();
        driver.switchTo().activeElement();
        sendKeysUsingAngularHack(RegisterHelper.emailField, RegisterHelper.emailAddress);
        sendKeysUsingAngularHack(RegisterHelper.firstNameField, RegisterHelper.firstName);
        sendKeysUsingAngularHack(RegisterHelper.lastNameField, RegisterHelper.lastName);
        sendKeysUsingAngularHack(RegisterHelper.passwordField, RegisterHelper.password);
        sendKeysUsingAngularHack(RegisterHelper.retypePasswordField, RegisterHelper.retypePassword);
        sendKeysUsingAngularHack(RegisterHelper.companyNameField, RegisterHelper.companyName);
        sendKeysUsingAngularHack(RegisterHelper.countrySelector, RegisterHelper.country);
        driver.findElement(By.xpath(RegisterHelper.signupButton)).click();
        Thread.sleep(3999);
        //asertu' de culoare
//		assert(driver.getPageSource().contains("User name already exists"));
        Assert.assertFalse(driver.findElement(By.xpath(RegisterHelper.signupButton)).isDisplayed(), "Register Failed");
        //*[@id="sign-up"]/div[4]
    }
      /*@AfterMethod public void afterMethod() {
          driver.close();
		  driver.quit(); 
		  }*/

    @BeforeMethod
    public void BeforeMethod() {
        System.setProperty("webdriver.chrome.driver", "/Users/radu/Downloads/chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //prod
        //driver.get("https://smp.protel.io/store/");
        //amdev
        //driver.get("https://amdev.protel.io:9443/store/");
        //SMPV2
        driver.get("http://amdev.protel.io:8080/SMP");
        //IS
        //driver.get("http://isdev.protel.io:8080/protelendpoint/");
    }

    public void sendKeysUsingAngularHack(String element, String keys) {
        int sizeOfKeys = keys.length();
        for (int i = 0; i < sizeOfKeys; i++) {
            driver.findElement(By.xpath(element)).sendKeys("" + keys.charAt(i));
            safeSleep(1);
        }
    }

    public void assertion_method_1(String xpath, String expected) {
        String Actual = driver.findElement(By.xpath(xpath)).getText();
//		System.out.println("--"+Actual+"--");
//		for(char c : Actual.toCharArray()){
//			System.out.println((int)c);
//		}
//		System.out.println("--"+expected+"--");
//		for(char c : expected.toCharArray()){
//			System.out.println((int)c);
//		}
        Assert.assertTrue(Actual.equals(expected));
        System.out.print("\n assertion_method_1() -> Part executed \n");
    }

    @Test
    public void registerWithoutEmail() {
        driver.findElement(By.xpath(RegisterHelper.signupIcon)).click();
        driver.switchTo().activeElement();
        //sendKeysUsingAngularHack(RegisterHelper.EmailField, RegisterHelper.EmailAddress);
        sendKeysUsingAngularHack(RegisterHelper.firstNameField, RegisterHelper.firstName);
        sendKeysUsingAngularHack(RegisterHelper.lastNameField, RegisterHelper.lastName);
        sendKeysUsingAngularHack(RegisterHelper.passwordField, RegisterHelper.password);
        sendKeysUsingAngularHack(RegisterHelper.retypePasswordField, RegisterHelper.retypePassword);
        sendKeysUsingAngularHack(RegisterHelper.companyNameField, RegisterHelper.companyName);
        sendKeysUsingAngularHack(RegisterHelper.countrySelector, RegisterHelper.country);
        driver.findElement(By.xpath(RegisterHelper.signupButton)).click();
        assertion_method_1(RegisterHelper.validationEmailField, RegisterHelper.validationEmail);
        System.out.println("Unable to register without Email address");
    }

    @Test
    public void registerWithoutLastName() {
        driver.findElement(By.xpath(RegisterHelper.signupIcon)).click();
        driver.switchTo().activeElement();
        sendKeysUsingAngularHack(RegisterHelper.emailField, RegisterHelper.emailAddress);
        sendKeysUsingAngularHack(RegisterHelper.firstNameField, RegisterHelper.firstName);
        //sendKeysUsingAngularHack(RegisterHelper.lastNameField, RegisterHelper.lastName);
        sendKeysUsingAngularHack(RegisterHelper.passwordField, RegisterHelper.password);
        sendKeysUsingAngularHack(RegisterHelper.retypePasswordField, RegisterHelper.retypePassword);
        sendKeysUsingAngularHack(RegisterHelper.companyNameField, RegisterHelper.companyName);
        sendKeysUsingAngularHack(RegisterHelper.countrySelector, RegisterHelper.country);
        driver.findElement(By.xpath(RegisterHelper.signupButton)).click();
        assertion_method_1(RegisterHelper.validationLastNameField, RegisterHelper.validationLastName);
        System.out.println("Unable to register without Last Name");
    }

    @Test
    public void registerWithoutFirstName() {
        driver.findElement(By.xpath(RegisterHelper.signupIcon)).click();
        driver.switchTo().activeElement();
        sendKeysUsingAngularHack(RegisterHelper.emailField, RegisterHelper.emailAddress);
        //sendKeysUsingAngularHack(RegisterHelper.FirstNameField, RegisterHelper.FirstName);
        sendKeysUsingAngularHack(RegisterHelper.lastNameField, RegisterHelper.lastName);
        sendKeysUsingAngularHack(RegisterHelper.passwordField, RegisterHelper.password);
        sendKeysUsingAngularHack(RegisterHelper.retypePasswordField, RegisterHelper.retypePassword);
        sendKeysUsingAngularHack(RegisterHelper.companyNameField, RegisterHelper.companyName);
        sendKeysUsingAngularHack(RegisterHelper.countrySelector, RegisterHelper.country);
        driver.findElement(By.xpath(RegisterHelper.signupButton)).click();
        assertion_method_1(RegisterHelper.validationFirstNameField, RegisterHelper.validationFirstName);
        System.out.println("Unable to register without First Name");
    }

    @Test
    public void registerWithoutPassword() {
        driver.findElement(By.xpath(RegisterHelper.signupIcon)).click();
        driver.switchTo().activeElement();
        sendKeysUsingAngularHack(RegisterHelper.emailField, RegisterHelper.emailAddress);
        sendKeysUsingAngularHack(RegisterHelper.firstNameField, RegisterHelper.firstName);
        sendKeysUsingAngularHack(RegisterHelper.lastNameField, RegisterHelper.lastName);
        //sendKeysUsingAngularHack(RegisterHelper.PasswordField, RegisterHelper.Password);
        sendKeysUsingAngularHack(RegisterHelper.retypePasswordField, RegisterHelper.retypePassword);
        sendKeysUsingAngularHack(RegisterHelper.companyNameField, RegisterHelper.companyName);
        sendKeysUsingAngularHack(RegisterHelper.countrySelector, RegisterHelper.country);
        driver.findElement(By.xpath(RegisterHelper.signupButton)).click();
        assertion_method_1(RegisterHelper.validationPasswordField, RegisterHelper.validationPassword);
        System.out.println("Unable to register without Password");
    }

    @Test
    public void registerWithoutConfirmPassword() {
        driver.findElement(By.xpath(RegisterHelper.signupIcon)).click();
        driver.switchTo().activeElement();
        sendKeysUsingAngularHack(RegisterHelper.emailField, RegisterHelper.emailAddress);
        sendKeysUsingAngularHack(RegisterHelper.firstNameField, RegisterHelper.firstName);
        sendKeysUsingAngularHack(RegisterHelper.lastNameField, RegisterHelper.lastName);
        sendKeysUsingAngularHack(RegisterHelper.passwordField, RegisterHelper.password);
        //sendKeysUsingAngularHack(RegisterHelper.retypePasswordField,RegisterHelper.retypePassword);
        sendKeysUsingAngularHack(RegisterHelper.companyNameField, RegisterHelper.companyName);
        sendKeysUsingAngularHack(RegisterHelper.countrySelector, RegisterHelper.country);
        driver.findElement(By.xpath(RegisterHelper.signupButton)).click();
        assertion_method_1(RegisterHelper.validationConfirmPasswordField, RegisterHelper.validationConfirmPassword);
        System.out.println("Unable to register without Password confirmation");
    }

    @Test
    public void registerWithoutCountry() {
        driver.findElement(By.xpath(RegisterHelper.signupIcon)).click();
        driver.switchTo().activeElement();
        sendKeysUsingAngularHack(RegisterHelper.emailField, RegisterHelper.emailAddress);
        sendKeysUsingAngularHack(RegisterHelper.firstNameField, RegisterHelper.firstName);
        sendKeysUsingAngularHack(RegisterHelper.lastNameField, RegisterHelper.lastName);
        sendKeysUsingAngularHack(RegisterHelper.passwordField, RegisterHelper.password);
        sendKeysUsingAngularHack(RegisterHelper.retypePasswordField, RegisterHelper.retypePassword);
        sendKeysUsingAngularHack(RegisterHelper.companyNameField, RegisterHelper.companyName);
        //sendKeysUsingAngularHack(RegisterHelper.CountrySelector,RegisterHelper.Country);
        driver.findElement(By.xpath(RegisterHelper.signupButton)).click();
        assertion_method_1(RegisterHelper.validationCountryField, RegisterHelper.validationCountryMessage);
        System.out.println("Unable to register without Country");
    }

    //@BeforeClass
    @Test
    public void registerWithoutCompanyName() {
        driver.findElement(By.xpath(RegisterHelper.signupIcon)).click();
        driver.switchTo().activeElement();
        sendKeysUsingAngularHack(RegisterHelper.emailField, RegisterHelper.emailAddress);
        sendKeysUsingAngularHack(RegisterHelper.firstNameField, RegisterHelper.firstName);
        sendKeysUsingAngularHack(RegisterHelper.lastNameField, RegisterHelper.lastName);
        sendKeysUsingAngularHack(RegisterHelper.passwordField, RegisterHelper.password);
        sendKeysUsingAngularHack(RegisterHelper.retypePasswordField, RegisterHelper.retypePassword);
        //sendKeysUsingAngularHack(RegisterHelper.companyNameField, RegisterHelper.companyName);
        sendKeysUsingAngularHack(RegisterHelper.countrySelector, RegisterHelper.country);
        driver.findElement(By.xpath(RegisterHelper.signupButton)).click();
        assertion_method_1(RegisterHelper.validationCompanyField, RegisterHelper.validationCompanyMessage);
        System.out.println("Unable to register without Company Name");
    }

    @Test
    public void integrationRequestDev() throws InterruptedException, AWTException {
        //Thread.sleep(2000);
        driver.findElement(By.xpath(IntegrationRequestHelper.signinIcon)).click();
        driver.switchTo().activeElement();
        sendKeysUsingAngularHack(IntegrationRequestHelper.amdevUsernameField, IntegrationRequestHelper.username);
        sendKeysUsingAngularHack(IntegrationRequestHelper.amdevPasswordField, IntegrationRequestHelper.password);
        driver.findElement(By.xpath(IntegrationRequestHelper.signInButtonDev)).click();
        driver.findElement(By.xpath(IntegrationRequestHelper.developmentButton)).click();
        driver.findElement(By.xpath(IntegrationRequestHelper.integrationRequestButton)).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath(IntegrationRequestHelper.newIntegrationRequestButton)).click();
        sendKeysUsingAngularHack(IntegrationRequestHelper.productNameField, IntegrationRequestHelper.productName);
        sendKeysUsingAngularHack(IntegrationRequestHelper.versionField, IntegrationRequestHelper.version);
        sendKeysUsingAngularHack(IntegrationRequestHelper.productFunctionalDescriptionField, IntegrationRequestHelper.productFunctionalDescription);
        Select dropdown = new Select(driver.findElement(By.xpath(IntegrationRequestHelper.selectPartnerNameField)));
        dropdown.selectByValue("9");
        WebElement we = driver.findElement(By.xpath(IntegrationRequestHelper.requestedStartDate));
        we.click();
        we.sendKeys(Keys.ENTER);
        driver.findElement(By.xpath(IntegrationRequestHelper.saveButton)).click();
    }

    @Test
    public void integrationRequestProd() throws InterruptedException, AWTException {
        //Thread.sleep(2000);
        driver.findElement(By.xpath(IntegrationRequestHelper.signinIcon)).click();
        driver.switchTo().activeElement();
        sendKeysUsingAngularHack(IntegrationRequestHelper.usernameField, IntegrationRequestHelper.username);
        sendKeysUsingAngularHack(IntegrationRequestHelper.passwordField, IntegrationRequestHelper.password);
        driver.findElement(By.xpath(IntegrationRequestHelper.signinButtonProd)).click();
        driver.findElement(By.xpath(IntegrationRequestHelper.developmentButton)).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath(IntegrationRequestHelper.integrationRequestButton)).click();
        Thread.sleep(1000);
        WebElement integrationRequestButton = driver.findElement(By.xpath(IntegrationRequestHelper.newIntegrationRequestButton));
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + integrationRequestButton.getLocation().y + ")");
        integrationRequestButton.click();

        //driver.findElement(By.xpath(IntegrationRequestHelper.newIntegrationRequestButton)).click();
        sendKeysUsingAngularHack(IntegrationRequestHelper.productNameField, IntegrationRequestHelper.productName);
        sendKeysUsingAngularHack(IntegrationRequestHelper.versionField, IntegrationRequestHelper.version);
        sendKeysUsingAngularHack(IntegrationRequestHelper.productFunctionalDescriptionField, IntegrationRequestHelper.productFunctionalDescription);
        Select dropdown = new Select(driver.findElement(By.xpath(IntegrationRequestHelper.selectPartnerNameField)));
        dropdown.selectByValue("20");
        WebElement we = driver.findElement(By.xpath(IntegrationRequestHelper.requestedStartDate));
        //WebElement integrationRequestButton= driver.findElement(By.xpath(IntegrationRequestHelper.newIntegrationRequestButton));
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + we.getLocation().y + ")");
        we.click();
        we.click();
        we.sendKeys(Keys.ENTER);
        driver.findElement(By.xpath(IntegrationRequestHelper.saveButton)).click();
    }

    //@BeforeClass
    @Test
    public void clearUserProd() throws InterruptedException {
        driver.navigate().to(CleanHelper.carbonProdUrl);
        sendKeysUsingAngularHack(CleanHelper.carbonUsernameField, CleanHelper.carbonUsername);
        sendKeysUsingAngularHack(CleanHelper.carbonPasswordField, CleanHelper.carbonProdPassword);
        driver.findElement(By.xpath(CleanHelper.carbonSigninButtom)).click();
        driver.findElement(By.xpath(CleanHelper.carbonUserList)).click();
        driver.findElement(By.xpath(CleanHelper.carbonUsers)).click();
        driver.findElement(By.xpath(CleanHelper.usernameSearch)).clear();
        sendKeysUsingAngularHack(CleanHelper.usernameSearch, CleanHelper.userToClean);
        driver.findElement(By.xpath(CleanHelper.cleanSearchButton)).click();
        driver.findElement(By.xpath(CleanHelper.deleteButton)).click();
        driver.findElement(By.xpath(CleanHelper.yesButton)).click();
        driver.findElement(By.xpath(CleanHelper.closeButton1)).click();
        driver.findElement(By.xpath(CleanHelper.closeButton2)).click();
        System.out.println("User successfully deleted");
    }

    @Test
    public void integrationApprovalDev() throws InterruptedException, AWTException {
        //Thread.sleep(2000);
        driver.findElement(By.xpath(IntegrationRequestHelper.signinIcon)).click();
        driver.switchTo().activeElement();
        sendKeysUsingAngularHack(IntegrationRequestHelper.amdevUsernameField, IntegrationRequestHelper.adminUsername);
        sendKeysUsingAngularHack(IntegrationRequestHelper.amdevPasswordField, IntegrationRequestHelper.adminDevPassword);
        driver.findElement(By.xpath(IntegrationRequestHelper.signInButtonDev)).click();
        driver.findElement(By.xpath(IntegrationRequestHelper.developmentButton)).click();
        driver.findElement(By.xpath(IntegrationRequestHelper.integrationRequestButton)).click();
        Thread.sleep(1000);
        WebElement e = driver.findElement(By.xpath(IntegrationRequestHelper.emailSearchField));
        Actions clicker = new Actions(driver);
        clicker.moveToElement(e).perform();
        sendKeysUsingAngularHack(IntegrationRequestHelper.emailSearchField, IntegrationRequestHelper.username);
        WebElement k = driver.findElement(By.xpath(IntegrationRequestHelper.dateSearchField));
        Actions clicker1 = new Actions(driver);
        clicker1.moveToElement(k).perform();
        sendKeysUsingAngularHack(IntegrationRequestHelper.dateSearchField, IntegrationRequestHelper.dateRequestEntered);
        Thread.sleep(5000);
        Actions clicker3 = new Actions(driver);
        WebElement z = driver.findElement(By.xpath(IntegrationRequestHelper.requestSelected));
        clicker3.moveToElement(z);
        clicker3.click();
        clicker3.build().perform();
        //Thread.sleep(3000);
        //Actions clicker2 = new Actions(driver);
        //WebElement we = driver.findElement(By.xpath(IntegrationRequestHelper.approvalDateSelector));
		/*clicker2.moveToElement(we);
		clicker2.click();
		clicker2.sendKeys(Keys.ENTER);
		clicker2.build().perform();
		driver.findElement(By.xpath(IntegrationRequestHelper.saveButtonAdmin)).click();*/
        //sendKeysUsingAngularHack(IntegrationRequestHelper.ApprovalDateSelector, IntegrationRequestHelper.DateRequestEntered);
        //driver.findElement(By.xpath(IntegrationRequestHelper.SaveButtonAdmin)).click();
        //sendKeysUsingAngularHack(IntegrationRequestHelper.EmailSearchField, IntegrationRequestHelper.username);
		/*driver.findElement(By.xpath(IntegrationRequestHelper.NewIntegrationRequestButton)).click();
		sendKeysUsingAngularHack(IntegrationRequestHelper.ProductNameField, IntegrationRequestHelper.ProductName);
		sendKeysUsingAngularHack(IntegrationRequestHelper.VersionField, IntegrationRequestHelper.Version);
		sendKeysUsingAngularHack(IntegrationRequestHelper.ProductFunctionalDescriptionField, IntegrationRequestHelper.ProductFunctionalDescription);
		Select dropdown = new Select(driver.findElement(By.xpath(IntegrationRequestHelper.SelectPartnerNameField)));
		dropdown.selectByValue("9");
		WebElement we = driver.findElement(By.xpath(IntegrationRequestHelper.RequestedStartDate));
		we.click();
		we.sendKeys(Keys.ENTER);
		driver.findElement(By.xpath(IntegrationRequestHelper.SaveButton)).click();*/
    }

    @Test
    public void integrationApprovalProd() throws InterruptedException, AWTException {
        //Thread.sleep(2000);
        driver.findElement(By.xpath(IntegrationRequestHelper.signinIcon)).click();
        driver.switchTo().activeElement();
        sendKeysUsingAngularHack(IntegrationRequestHelper.usernameField, IntegrationRequestHelper.adminUsername);
        sendKeysUsingAngularHack(IntegrationRequestHelper.passwordField, IntegrationRequestHelper.adminProdPassword);
        driver.findElement(By.xpath(IntegrationRequestHelper.signinButtonProd)).click();
        driver.findElement(By.xpath(IntegrationRequestHelper.developmentButton)).click();
        driver.findElement(By.xpath(IntegrationRequestHelper.integrationRequestButton)).click();
        Thread.sleep(1000);
        WebElement e = driver.findElement(By.xpath(IntegrationRequestHelper.emailSearchField));
        Actions clicker = new Actions(driver);
        clicker.moveToElement(e).perform();
        sendKeysUsingAngularHack(IntegrationRequestHelper.emailSearchField, IntegrationRequestHelper.username);
        WebElement k = driver.findElement(By.xpath(IntegrationRequestHelper.dateSearchField));
        Actions clicker1 = new Actions(driver);
        clicker1.moveToElement(k).perform();
        sendKeysUsingAngularHack(IntegrationRequestHelper.dateSearchField, IntegrationRequestHelper.dateRequestEntered);
        Thread.sleep(5000);
        WebElement requestSelected = driver.findElement(By.xpath(IntegrationRequestHelper.requestSelected));
        Actions actions = new Actions(driver);
        ((JavascriptExecutor) driver).executeScript("$('#listIR tbody tr:first td:eq(1)').click();");
        WebElement bz = driver.findElement(By.xpath(IntegrationRequestHelper.approvalDateSelector));
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + bz.getLocation().y + ")");
        bz.click();
        bz.sendKeys(Keys.ENTER);
        WebElement savebutton = driver.findElement(By.xpath(IntegrationRequestHelper.saveButtonAdmin));
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + savebutton.getLocation().y + ")");
        savebutton.click();
    }

    @Test
    public void integrationsetdateProd() throws InterruptedException, AWTException {
        //Thread.sleep(2000);
        driver.findElement(By.xpath(IntegrationRequestHelper.signinIcon)).click();
        driver.switchTo().activeElement();
        sendKeysUsingAngularHack(IntegrationRequestHelper.usernameField, IntegrationRequestHelper.adminUsername);
        sendKeysUsingAngularHack(IntegrationRequestHelper.passwordField, IntegrationRequestHelper.adminProdPassword);
        driver.findElement(By.xpath(IntegrationRequestHelper.signinButtonProd)).click();
        driver.findElement(By.xpath(IntegrationRequestHelper.developmentButton)).click();
        driver.findElement(By.xpath(IntegrationRequestHelper.integrationRequestButton)).click();
        Thread.sleep(1000);
        WebElement e = driver.findElement(By.xpath(IntegrationRequestHelper.emailSearchField));
        Actions clicker = new Actions(driver);
        clicker.moveToElement(e).perform();
        sendKeysUsingAngularHack(IntegrationRequestHelper.emailSearchField, IntegrationRequestHelper.username);
        WebElement k = driver.findElement(By.xpath(IntegrationRequestHelper.dateSearchField));
        Actions clicker1 = new Actions(driver);
        clicker1.moveToElement(k).perform();
        sendKeysUsingAngularHack(IntegrationRequestHelper.dateSearchField, IntegrationRequestHelper.dateRequestEntered);
        Thread.sleep(5000);
        WebElement requestSelected = driver.findElement(By.xpath(IntegrationRequestHelper.requestSelected));
        Actions actions = new Actions(driver);
        ((JavascriptExecutor) driver).executeScript("$('#listIR tbody tr:first td:eq(1)').click();");
        WebElement ko = driver.findElement(By.xpath(IntegrationRequestHelper.kickOffDate));
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + ko.getLocation().y + ")");
        ko.click();
        ko.sendKeys(Keys.ENTER);
        WebElement savebutton = driver.findElement(By.xpath(IntegrationRequestHelper.saveButtonAdmin));
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + savebutton.getLocation().y + ")");
        savebutton.click();
    }

    @Test
    public void registerIAM() throws InterruptedException {
        driver.findElement(By.xpath(RegisterHelper.iamRegisterIcon)).click();
//		driver.switchTo().activeElement();
        //Integer size = driver.findElements(By.tagName("iframe")).size();
        sendKeysUsingAngularHack(RegisterHelper.iamEmailField, RegisterHelper.emailAddress);
        sendKeysUsingAngularHack(RegisterHelper.iamPhoneField,RegisterHelper.phone);
//        String max_length = driver.findElement(By.xpath(RegisterHelper.iamLastNameField)).getAttribute("maxlength");
//        System.out.println(max_length);
        sendKeysUsingAngularHack(RegisterHelper.iamFirstNameField, RegisterHelper.firstName);
        sendKeysUsingAngularHack(RegisterHelper.iamLastNameField, RegisterHelper.lastName);
        sendKeysUsingAngularHack(RegisterHelper.iamPasswordField, RegisterHelper.password);
        sendKeysUsingAngularHack(RegisterHelper.iamConfirmPasswordField, RegisterHelper.retypePassword);
        sendKeysUsingAngularHack(RegisterHelper.iamCompanyNameField, RegisterHelper.companyName);
        Thread.sleep(3000);
        WebElement elementToClick = driver.findElement(By.xpath(RegisterHelper.iamCountrySelect));
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + elementToClick.getLocation().y + ")");
        elementToClick.click();

        WebElement country = driver.findElement(By.xpath(RegisterHelper.iamCountrySelected));
        Actions actions = new Actions(driver);
        actions.moveToElement(country);
        actions.click();
        actions.build().perform();

//		WebElement cityField=driver.findElement(By.xpath(RegisterHelper.iamCityField));
//		((JavascriptExecutor)driver).executeScript("window.scrollTo(0,"+cityField.getLocation().y+")");
//		cityField.
//		Boolean ok=cityField.isSelected();
//		System.out.println(ok);
        //sendKeysUsingAngularHack(RegisterHelper.iamCityField, RegisterHelper.iamCity);
        //driver.findElement(By.xpath(driver)).click();
        driver.findElement(By.xpath(RegisterHelper.iamSignupButton)).click();
        Thread.sleep(3999);
//		//asertu' de culoare
////		assert(driver.getPageSource().contains("User name already exists"));
//		Assert.assertFalse(driver.findElement(By.xpath(RegisterHelper.signupButton)).isDisplayed(), "Register Failed");
//		//*[@id="sign-up"]/div[4]
    }


    @Test
    public void smpV2integrationRequest() throws InterruptedException, AWTException {
        //Thread.sleep(2000);
        sendKeysUsingAngularHack(SMPv2IntegrationRequestHelper.smpV2UsernameField, SMPv2IntegrationRequestHelper.username);
        sendKeysUsingAngularHack(SMPv2IntegrationRequestHelper.smpV2PasswordField, SMPv2IntegrationRequestHelper.password);
        driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.smpV2LoginButton)).click();
        Thread.sleep(500);
        assertion_method_1(SMPv2IntegrationRequestHelper.UsernameCheck, "person " + SMPv2IntegrationRequestHelper.username);
        Thread.sleep(500);
        driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.developmentButton)).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.integrationRequestButton)).click();
        Thread.sleep(2000);
        javaScriptElementClick(SMPv2IntegrationRequestHelper.newIntegrationRequestButton);
        sendKeysUsingAngularHack(SMPv2IntegrationRequestHelper.productNameField, SMPv2IntegrationRequestHelper.productName);
        sendKeysUsingAngularHack(SMPv2IntegrationRequestHelper.versionField, SMPv2IntegrationRequestHelper.version);
        sendKeysUsingAngularHack(SMPv2IntegrationRequestHelper.productFunctionalDescriptionField, SMPv2IntegrationRequestHelper.productFunctionalDescription);
        Thread.sleep(2000);
        driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.selectPartnerNameField)).click();
        Thread.sleep(4000);
        //driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.partnerName)).click();

        //javaScriptElementClick(SMPv2IntegrationRequestHelper.partnerName);
        Thread.sleep(4000);
        sleepAndPerformActions(Keys.TAB);
        javaScriptElementClick(SMPv2IntegrationRequestHelper.typeofIntegrationSelectButton);
        sleepAndPerformActions(Keys.TAB);
        javaScriptElementClick(SMPv2IntegrationRequestHelper.technicalTypeSelectButton);
        sleepAndPerformActions(Keys.TAB);
        javaScriptElementClick(SMPv2IntegrationRequestHelper.forWhichPMSSelectionButton);
        sleepAndPerformActions(Keys.TAB);
        javaScriptElementClick(SMPv2IntegrationRequestHelper.dataFlowSelectButton);
        sleepAndPerformActions(Keys.TAB);
        javaScriptElementClick(SMPv2IntegrationRequestHelper.transportTypeSelectButton);
        sleepAndPerformActions(Keys.TAB);
        javaScriptElementClick(SMPv2IntegrationRequestHelper.pCIStatusSelectionButton);
        sleepAndPerformActions(Keys.TAB);
//        //javaScriptElementClick(SMPv2IntegrationRequestHelper.today);
        //driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.today)).click();
        sleepAndPerformActions(Keys.TAB);
        javaScriptElementClick (SMPv2IntegrationRequestHelper.saveButton);
        //WebElement calendar = driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.selectDate));
//        WebElement saveButton = driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.saveButton));
//        Actions aa = new Actions (driver);
//        aa.moveToElement (saveButton);
//        aa.click ();
//        Thread.sleep(1750);
//        //driver.findElement(By.xpath("/html/body/app-root/content/app-new-integration-request/form/div[2]/div[2]/div/div[3]")).click();
//        driver.findElement(By.xpath("/html/body/app-root/content/app-new-integration-request/form/div[2]/div[2]/div/div[3]/button/span")).click();
    }


    @Test
    public void smpV2CreateBulkTransferService() throws InterruptedException, AWTException {
        //Thread.sleep(2000);
        sendKeysUsingAngularHack(SMPV2BulkTransferServiceHelper.smpV2UsernameField, SMPV2BulkTransferServiceHelper.username);
        sendKeysUsingAngularHack(SMPV2BulkTransferServiceHelper.smpV2PasswordField, SMPV2BulkTransferServiceHelper.password);
        driver.findElement(By.xpath(SMPV2BulkTransferServiceHelper.smpV2LoginButton)).click();
        Thread.sleep(500);
        assertion_method_1(SMPv2IntegrationRequestHelper.UsernameCheck, "person " + SMPv2IntegrationRequestHelper.username);
        Thread.sleep(500);
        driver.findElement(By.xpath(SMPV2BulkTransferServiceHelper.AdministrationButton)).click();
        Thread.sleep(4000);
        javaScriptElementClick(SMPV2BulkTransferServiceHelper.BulkTransferlink);
        Thread.sleep(1000);
        javaScriptElementClick(SMPV2BulkTransferServiceHelper.createNewJob);
        driver.findElement(By.xpath(SMPV2BulkTransferServiceHelper.routingStage)).click();
        sleepAndPerformActions(Keys.TAB);
        driver.findElement(By.xpath(SMPV2BulkTransferServiceHelper.hotelId)).click();
        sleepAndPerformActions(Keys.TAB);
        driver.findElement(By.xpath(SMPV2BulkTransferServiceHelper.sourceService)).click();
        sleepAndPerformActions(Keys.TAB);
        javaScriptElementClick(SMPV2BulkTransferServiceHelper.targetService);
        sleepAndPerformActions(Keys.TAB);
        javaScriptElementClick(SMPV2BulkTransferServiceHelper.messageType);
        sleepAndPerformActions(Keys.TAB);
        javaScriptElementClick(SMPV2BulkTransferServiceHelper.startDate);
        sleepAndPerformActions(Keys.TAB);
        javaScriptElementClick(SMPV2BulkTransferServiceHelper.endDateSelection);
        javaScriptElementClick(SMPV2BulkTransferServiceHelper.endDate);
        sleepAndPerformActions(Keys.TAB);
        sendKeysUsingAngularHack(SMPV2BulkTransferServiceHelper.recordsPerMessage,SMPV2BulkTransferServiceHelper.numberOfRecords);
        sendKeysUsingAngularHack(SMPV2BulkTransferServiceHelper.pausePerMessage,SMPV2BulkTransferServiceHelper.numberOfPauses);
        javaScriptElementClick(SMPV2BulkTransferServiceHelper.saveJobButton);






//        //Click on new Integration request Button
//        WebElement elementToClick = driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.newIntegrationRequestButton));
//        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + elementToClick.getLocation().y + ")");
//        elementToClick.click();
//        sendKeysUsingAngularHack(SMPv2IntegrationRequestHelper.productNameField, SMPv2IntegrationRequestHelper.productName);
//        sendKeysUsingAngularHack(SMPv2IntegrationRequestHelper.versionField, SMPv2IntegrationRequestHelper.version);
//        sendKeysUsingAngularHack(SMPv2IntegrationRequestHelper.productFunctionalDescriptionField, SMPv2IntegrationRequestHelper.productFunctionalDescription);
//        driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.selectPartnerNameField)).click();
//        sleepAndPerformActions(Keys.TAB);
//        WebElement typeOfIntegration = driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.typeofIntegrationSelectButton));
//        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + typeOfIntegration.getLocation().y + ")");
//        typeOfIntegration.click();
//        sleepAndPerformActions(Keys.TAB);
//        WebElement integrationTechnicalType = driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.technicalTypeSelectButton));
//        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + integrationTechnicalType.getLocation().y + ")");
//        integrationTechnicalType.click();
//        sleepAndPerformActions(Keys.TAB);
//        WebElement whichPMS = driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.forWhichPMSSelectionButton));
//        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + whichPMS.getLocation().y + ")");
//        whichPMS.click();
//        sleepAndPerformActions(Keys.TAB);
//        WebElement dataflow = driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.dataFlowSelectButton));
//        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + dataflow.getLocation().y + ")");
//        dataflow.click();
//        //javaScriptElementClick(By.xpath(SMPv2IntegrationRequestHelper.dataFlowSelectButton));
//        sleepAndPerformActions(Keys.TAB);
//        WebElement transportType = driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.transportTypeSelectButton));
//        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + transportType.getLocation().y + ")");
//        transportType.click();
//        sleepAndPerformActions(Keys.TAB);
//        WebElement pCIStatusSelection = driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.pCIStatusSelectionButton));
//        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + pCIStatusSelection.getLocation().y + ")");
//        pCIStatusSelection.click();
//        sleepAndPerformActions(Keys.TAB);
//        WebElement mumu = driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.pCIStatusSelectionButton));
//        //variantamerge
//        driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.today)).click();
//        WebElement calendar = driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.selectDate));
//        driver.switchTo().parentFrame();
//        WebElement saveButton = driver.findElement(By.xpath(SMPv2IntegrationRequestHelper.saveButton));
//        Actions actions = new Actions(driver);
//        actions.moveToElement(saveButton);
//        actions.click();
//        Thread.sleep(1750);
//        //driver.findElement(By.xpath("/html/body/app-root/content/app-new-integration-request/form/div[2]/div[2]/div/div[3]")).click();
//        driver.findElement(By.xpath("/html/body/app-root/content/app-new-integration-request/form/div[2]/div[2]/div/div[3]/button/span")).click();
    }
    private void sleepAndPerformActions(Keys... keys) {
        safeSleep(500);

        Actions action = new Actions(driver);
        action.click();
        action.sendKeys(keys);
        action.build().perform();
    }

    private void javaScriptElementClick(String xpath){
        safeSleep(1000);
        WebElement we=driver.findElement(By.xpath(xpath));
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + we.getLocation().y + ")");
        we.click();

    }
}

	
	
	

