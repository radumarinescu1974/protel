package protelTest;

public class SMPv2IntegrationRequestHelper {
    //variables
    public static final String username = "radu.marinescu@iello.ro";
    public static final String password = "Mujix21";
    public static final String partnerNameSelection = "Rategain";
    public static final String adminUsername = "admin@wso2.com";
    public static final String adminDevPassword = "99s63U@GthE9";
    public static final String adminProdPassword = "AwgTUwMGqWRqrRxh";
    public static final String productName = "test17oct";
    public static final String version = "1.0";
    public static final String productFunctionalDescription = "UltimateNameDescriptionTest";
    public static final String dateRequestEntered = "05-Sep-2017";
    public static final String today=".//*[@id='md-datepicker-0']/div[2]/md-month-view/table/tbody/tr[3]/td[3]";


    //constants
    public static final String smpV2UsernameField="//INPUT[@id='md-input-0']";
    public static final String smpV2PasswordField="//INPUT[@id='md-input-1']";
    public static final String UsernameCheck="//BUTTON[@_ngcontent-c8='']";
    public static final String smpV2LoginButton="/html/body/app-root/login/div/div/div/form/div/div/div[3]/div[3]/div[2]/button";
    public static final String amdevUsernameField = "//*[@id=\"md-input-0\"]";
    public static final String amdevPasswordField = "//*[@id=\"md-input-1\"]";
    public static final String signinIcon = "/html/body/header/nav/div[3]/ul/ul/li[2]/a";
    public static final String usernameField = "//*[@id=\"username\"]";
    public static final String passwordField = "//*[@id=\"password\"]";

    public static final String signInButtonDev = "/html/body/app-root/login/div/div/div/form/div/div/div[3]/div[3]/div[2]/button/span";
    public static final String developmentButton = "/html/body/app-root/top-navi/md-toolbar/div/mat-toolbar-row/div[1]/development/button";
    public static final String integrationRequestButton = "/html/body/div/div[2]/div/div/button[3]";
    public static final String newIntegrationRequestButton = "/html/body/app-root/div[1]/div/div/app-integration-request-list/div/div[2]/button";
    public static final String productNameField = "//*[@id=\"md-input-3\"]";
    public static final String versionField = "//*[@id=\"md-input-4\"]";
    public static final String productFunctionalDescriptionField = "//INPUT[@id='md-input-7']";
    public static final String selectPartnerNameField = "/html/body/app-root/div[1]/div/div/app-new-integration-request/form/div[1]/div[2]/div/fieldset[1]/div[1]/div[1]";
    public static final String partnerName = "//MD-OPTION[@id='md-option-4']";
    public static final String typeofIntegrationSelectButton="(//DIV[@_ngcontent-c21=''])[33]";
    public static final String typeofIntegrationSelect="";
    public static final String technicalTypeSelectButton="(//DIV[@class='mat-select-trigger'])[3]";
    public static final String dataFlowSelectButton="(//DIV[@class='mat-select-trigger'])[5]";
    public static final String pCIStatusSelectionButton="(//DIV[@class='mat-select-trigger'])[7]";
    public static final String forWhichPMSSelectionButton="(//DIV[@class='mat-select-trigger'])[4]";
    public static final String transportTypeSelectButton="(//DIV[@class='mat-select-trigger'])[6]";
    public static final String selectDate="//*[@id=\"md-input-17\"]";
    public static final String saveButton="/html/body/app-root/div[1]/div/div/app-new-integration-request/form/div[2]/div[2]/div/div[3]/button";
    public static final String priorityScoreField="//*[@id=\"md-input-17\"]";
    public static final String priorityScore="10";
    public static final String requestedStartDate = "";
    public static final String requestedDate = "";
    //public static final String saveButton = "";
    public static final String emailSearchField = "";
    public static final String dateSearchField = "";
    public static final String requestSelected = "";
    public static final String approvalDateSelector = "";
    public static final String saveButtonAdmin = "";
    public static final String signinButtonProd = "";
}
