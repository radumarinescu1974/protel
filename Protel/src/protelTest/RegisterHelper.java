package protelTest;

public class RegisterHelper {
    //variables
    public static final String emailAddress = "radu.marinescu@iello.ro";
    public static final String firstName = "dan radü-test";
    public static final String lastName = "marinescü test-mail";
    public static final String password = "xxx";
    public static final String retypePassword = "xxx";
    public static final String companyName = "Iello-DEV";
    public static final String signupIcon = "/html/body/header/nav/div[3]/ul/ul/li[1]/a";
    public static final String emailField = "//*[@id=\"5.0claimUri\"]";
    public static final String country="Romania";
    public static final String phone="+xxxxx";
    //constants
    public static final String iamPhoneField = "//*[@id=\"md-input-3\"]";
    public static final String iamRegisterIcon = "//A[@_ngcontent-c1=''][text()='Register now']";
    public static final String iamEmailField = "//*[@id=\"md-input-2\"]";
    public static final String iamFirstNameField = "//*[@id=\"md-input-5\"]";
    public static final String iamLastNameField = "//*[@id=\"md-input-6\"]";
    public static final String iamPasswordField = "//*[@id=\"md-input-7\"]";
    public static final String iamConfirmPasswordField = "//*[@id=\"md-input-8\"]";
    public static final String iamCompanyNameField = "//*[@id=\"md-input-9\"]";
    public static final String iamCountrySelect = "/html/body/app-root/signup/div/div/div/form/div/div/div[5]/div[4]/div[2]/md-select/div/span[1]";
    public static final String iamCountrySelected = "//*[@id=\"md-option-4\"]";
    public static final String iamCityField = "//*[@id=\"md-input-11\"]";
    public static final String iamCity = "Tallinn";
    public static final String iamSignupButton = "//SPAN[@class='mat-button-wrapper'][text()='Sign Up\n" +
            "                  ']";
    public static final String firstNameField = "//*[@id=\"0.0claimUri\"]";
    public static final String passwordField = "//*[@id=\"newPassword\"]";
    public static final String lastNameField = "//*[@id=\"1.0claimUri\"]";
    public static final String retypePasswordField = "//*[@id=\"newPasswordConfirm\"]";
    public static final String companyNameField = "//*[@id=\"2.0claimUri\"]";
    public static final String countrySelector = "//*[@id=\"3.0claimUri\"]";
    public static final String signupButton = "//*[@id=\"signUpBtn\"]";
    public static final String validationEmailField = "//*[@id=\"sign-up\"]/div[1]/div[2]/div[1]/div/span[1]";
    public static final String validationEmail = "This field is required.";
    public static final String validationFirstNameField = "//*[@id=\"sign-up\"]/div[1]/div[2]/div[2]/div/span[1]";
    public static final String validationFirstName = "This field is required.";
    public static final String validationLastNameField = "//*[@id=\"sign-up\"]/div[1]/div[3]/div[2]/div/span[1]";
    public static final String validationLastName = "This field is required.";
    public static final String validationPasswordField = "//*[@id=\"sign-up\"]/div[1]/div[3]/div[1]/div[1]/span[1]";
    public static final String validationPassword = "Enter a more secure password";
    public static final String validationCountryField = "//*[@id=\"sign-up\"]/div[2]/div/div[3]/div[2]/div/span[1]";
    public static final String validationCountryMessage = "This field is required.";
    public static final String validationCompanyField = "//*[@id=\"sign-up\"]/div[2]/div/div[1]/div[1]/div/span[1]";
    public static final String validationCompanyMessage = "This field is required.";
    public static final String validationConfirmPasswordField = "//*[@id=\"sign-up\"]/div[1]/div[4]/div[1]/div/span[1]";
    public static final String validationConfirmPassword = "Confirm Password";
    public static final String bannerError = "//*[@id=\"sign-up\"]/div[4]";
}
