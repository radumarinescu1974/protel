package protelTest;

public class IntegrationRequestHelper {
    //variables
    public static final String username = "radu.marinescu@iello.ro";
    public static final String password = "xxxx";
    public static final String partnerNameSelection = "Rategain";
    public static final String adminUsername = "admin@wso2.com";
    public static final String adminDevPassword = "99s63U@GthE9";
    public static final String adminProdPassword = "AwgTUwMGqWRqrRxh";
    public static final String productName = "ProductNameTest1";
    public static final String version = "1.0";
    public static final String productFunctionalDescription = "ProductNameDescriptionTest";
    public static final String dateRequestEntered = "05-Oct-2017";

    //constants
    public static final String amdevUsernameField = "//*[@id=\"md-input-0\"]";
    public static final String amdevPasswordField = "//*[@id=\"md-input-1\"]";
    public static final String signinIcon = "/html/body/header/nav/div[3]/ul/ul/li[2]/a";
    public static final String usernameField = "//*[@id=\"username\"]";
    public static final String passwordField = "//*[@id=\"password\"]";
    public static final String signInButtonDev = "/html/body/app-root/login/div/div/div/form/div/div/div[3]/div[3]/div[2]/button/span";
    public static final String developmentButton = "//A[@href='#'][text()='Development ']";
    public static final String integrationRequestButton = "//A[@href='/store/site/pages/integration-request/integrationRequestList.jag'][text()='Integration Requests']";
    public static final String newIntegrationRequestButton = "/html/body/div[1]/div/div[2]/div[2]/section[2]/div/div[2]/form/div[2]/div/div/div/button";
    public static final String productNameField = "//INPUT[@id='partnName']";
    public static final String versionField = "//*[@id=\"partnVersion\"]";
    public static final String productFunctionalDescriptionField = "//INPUT[@id='prodFuctDescr']";
    public static final String selectPartnerNameField = "//*[@id=\"partnerName\"]";
    public static final String partnerName = "//*[@id=\"partnerName\"]/option[2]";
    public static final String requestedStartDate = "//*[@id=\"requestedstartdate\"]";
    public static final String requestedDate = "/html/body/div[6]/div[1]/table/tbody/tr[4]/td[6]";
    public static final String kickOffDate="//*[@id=\"kickDate\"]";
    public static final String saveButton = "//BUTTON[@id='btnSaveIR']";
    public static final String emailSearchField = "//*[@id=\"listIR_wrapper\"]/div[2]/div/div[1]/div[1]/div/table/thead/tr[1]/th[8]/input";
    public static final String dateSearchField = "//*[@id=\"listIR_wrapper\"]/div[2]/div/div[1]/div[1]/div/table/thead/tr[1]/th[7]/input";
    public static final String requestSelected = "//*[@id=\"listIR\"]/tbody/tr/td[1]";
    public static final String approvalDateSelector = "//*[@id=\"integrationApprovalDate\"]";
    public static final String saveButtonAdmin = "//*[@id=\"btnSaveIR\"]";
    public static final String signinButtonProd = "//*[@id=\"loginBtn\"]";
    //*[@id="integrationApprovalDate"]
}
