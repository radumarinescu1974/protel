package protelTest;

public class SMPV2BulkTransferServiceHelper {
    //variables
    public static final String username = "radu.marinescu@iello.ro";
    public static final String password = "Mujix21";
    //constants
    public static final String smpV2UsernameField="//INPUT[@id='md-input-0']";
    public static final String smpV2PasswordField="//INPUT[@id='md-input-1']";
    public static final String UsernameCheck="//BUTTON[@_ngcontent-c8='']";
    public static final String smpV2LoginButton="/html/body/app-root/login/div/div/div/form/div/div/div[3]/div[3]/div[2]/button";
    public static final String AdministrationButton="/html/body/app-root/top-navi/md-toolbar/div/mat-toolbar-row/div[1]/administration/button";
    public static final String BulkTransferlink="//*[@id=\"cdk-overlay-0\"]/div/div/a[1]/div";
    public static final String createNewJob="//BUTTON[@_ngcontent-c13='']";
    public static final String routingStage="//*[@id=\"stage\"]/div";
    public static final String hotelId="/html/body/app-root/content/app-job-create/form/div/div[2]/div[2]/md-select/div";
    public static final String sourceService="/html/body/app-root/content/app-job-create/form/div/div[3]/div[1]/md-select/div/span[1]";
    public static final String targetService="/html/body/app-root/content/app-job-create/form/div/div[3]/div[2]/md-select/div";
    public static final String messageType="/html/body/app-root/content/app-job-create/form/div/div[4]/div/md-select/div";
    public static final String startDate="//*[@id=\"md-datepicker-0\"]/div[2]/md-month-view/table/tbody/tr[3]/td[1]/div";
    public static final String endDateSelection="//*[@id=\"md-input-3\"]";
    public static final String endDate="//*[@id=\"md-datepicker-1\"]/div[2]/md-month-view/table/tbody/tr[3]/td[1]/div";
    public static final String recordsPerMessage="//*[@id=\"md-input-4\"]";
    public static final String numberOfRecords="200";
    public static final String pausePerMessage="//*[@id=\"md-input-5\"]";
    public static final String numberOfPauses="50";
    public static final String saveJobButton="/html/body/app-root/content/app-job-create/form/div/div[7]/div[1]/button";
}
